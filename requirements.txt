torch
pytorch-ignite
spacy
transformers==3.0.2
tensorboardX==2.1
tensorflow  # for tensorboardX
