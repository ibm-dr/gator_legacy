## GATOR: The Goal-oriented Autonomous Dialogue System

| Tags     | License    | Datasets |
| ---------|------------|----------|
| Dialog   | MIT      | [Persuasion For Good](https://gitlab.com/ucdavisnlp/persuasionforgood)
| | | [AntiScam](https://gitlab.com/ucdavisnlp/antiscam) |


### Model description

We propose to develop a new type of human-machine dialogue system that uses deep  learning  technologies  (such  as  transformers)  to  learn  how  to  recognize  and generate dialogue plans, i.e., semantic and pragmatic structures that represent one party’s goals and intentions, as well as the impact these are having on the other party. Unlike the current transformer-driven chat bots, the core learning is not to transform one language expression (input) into another language expression (response) but instead to construct a response plan that would properly address the plan in the input and the history of interaction. Consequently, the learning process takes three types of information: (1) the input utterance; (2) its semantic-pragmatic plan, i.e., the plan that was used to produce the utterance, and (3) the history of interaction up to this point. Furthermore, the cumulative history of the dialogue is not merely the memory of the utterances exchanged earlier, but it captures, in a condensed semantic form, the evolving state of the parties’ objectives as well as the emerging sociolinguistic behavioral patterns of both (all) parties.


### Installation

To install and use the training and inference scripts please clone the repo and install the requirements:

```bash
git clone https://bitbucket.org/ibm-dr/gator
cd gator
pip install -r requirements.txt
python -m spacy download en
```


### Running the chat
You can run the [HuggingFace implementation of Microsoft's DialoGPT](https://huggingface.co/transformers/model_doc/dialogpt.html) (which is actually an implementation of HuggingFace GPT-2 with fine-tuning):

```bash
python dgpt.py
```

You can run the cleaned [HuggingFace code for the NeurIPS 2018 ConvAI2 dialog competition](https://github.com/huggingface/transfer-learning-conv-ai):

```bash
python convai.py
```

##### Limitations and bias

Provide examples of latent issues and potential remediations.

### Training data

Description of the data used to train the model.
TODO - add a link to the pre-trained model card or repository with description of the pre-training data. -> (GPT-2, BERT, DialoGPT, etc.)

### Training procedure

Preprocessing, hardware used, hyperparameters...